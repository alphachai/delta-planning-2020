# Delta LTP

Long-term planning of Delta Squad (SRE) projects for FY 2019-2020

To understand dependencies visually, please refer to our [miro boards](#sources).

### Table of Contents

* [Legend](#legend)
* [FAQ](#faq)
* [Sources](#sources)
* [Projects](#projects)
* [Project Details](#project-details)

### Legend

| | Status |
| -: | - |
| 🧪 | Unknown, prerequisites or experimentation needed |
| 🟥 | Planning: Not Started |
| 🟨 | Planning: In-Progress or Needs Grooming |
| 🟩 | Planning: Complete |
| 💻 | In Progress |
| (?) | We don't know how to estimate this yet **OR** some portion of this work hasn't been discovered yet. |

## FAQ

##### What degree of certainty are these estimates at?

If there are no (?) or 🧪 symbols, the estimate is roughly 80% confidence. Of course, delta is relied on by other teams to provide on-demand support and improvements as their projects require, so *our estimates will be too conservative*.

##### What does a question mark mean?

1. We don't know how to estimate this yet.
2. Some portion of this work hasn't been discovered yet.

##### What do these estimates translate to?

The amount of time it would take one average developer on our team to accomplish the work. We expect most epics to be led by a single person and multiple epics to be in progress at any given time.

## Sources

* [2019 11 LTP](https://miro.com/app/board/o9J_kwKBIbE=/)
* [2019 10 Rough Long-Term Roadmap](https://miro.com/app/board/o9J_kwv0di4=/) (*outdated*)
* [2019 10 Epic Planning](https://miro.com/app/board/o9J_kxTVeM0=/) (*outdated*)

## Projects

| Project | Status | 🕙 |
| - | - | - |
| [Kill Chef](#kill-chef) | 💻 | 3d |
| [Refactor Bastion VPN](#refactor-bastion-vpn) | 💻 | 3w |
| [Development Tooling](#development-tooling) | 🟩 | 4w |
| [Git Tooling](#git-tooling) | 🟩 | 4w |
| [Artifactory](#artifactory) | 🟩 | 2w 1d |
| [Refactor CI/CD](#refactor-cicd) | 🟩 🧪 | 8w (?) |
| [Elasticsearch Refactor](#elasticsearch-refactor) | 💻 | 2w 3d |
| [Elasticsearch CI/CD](#elasticsearch-cicd) | 🟩 🧪 | 2w (?) |
| [Logging Refactor](#logging-refactor) | 🟩 🧪 | 2w (?) |
| [Update Omni ES Deployment](#update-omni-es-deployment) | 🟩 | 1w |
| [Setup Tracing Service](#setup-tracing-service) | 🟩 🧪 | 4w (?) |
| [Horizontally Scaled Prometheus](#horizontally-scaled-prometheus) | 🟩 🧪 | 4w (?) |
| [Monitoring Visibility](#monitoring-visibility) | 🟩 | 2w 4d |
| [Monitoring Alerting](#monitoring-alerting) | 🟩 🧪 | 2w (?) |
| [Accessible Documentation](#accessible-documentation) | 🟩 | 3w 3d |
| [Refactor TF CLI](#refactor-tf-cli) | 🟩 | 2w |
| [Upgrade to TF 0.12](#upgrade-to-tf-012) | 🟩 | 4w |
| [Terraform CI/CD](#terraform-cicd) | 🟩 🧪 | 2w (?) |
| [Organize AWS Accounts](#organize-aws-accounts) | 🟨 🧪 |  |
| [Create AWS Budgets](#create-aws-budgets) | 🟩 | 1w |
| [Create AWS Reports](#create-aws-budgets) | 🟩 🧪 | 2d (?) |
| [SPIKE: Integration Testing](#spike-integration-testing) | 💻 |  |
| [Testing Methodology](#testing-methodology) | 🟩 | 2w 2d |
| [Terraform Maintenance](#terraform-maintenance) | 🟨 | 4w 3d |
| [Terraform Unit Testing](#terraform-unit-testing) | 🟨 🧪 |  |
| [Terraform Cookiecutter](#terraform-cookiecutter) | 🟩 | 1w 1d |
| [Terraform ECS Autoscaling](#terraform-ecs-autoscaling) | 🟩 | 2w |
| [Terraform SQS Autoscaler](#terraform-sqs-autoscaler) | 🟩 | 3d |
| [Cloud Gateways And IDP](#cloud-gateways-and-idp) | 💻 | 4w (?) |
| [Provision Opsgenie via TF](#provision-opsgenie-via-tf) | 🟩 | 2w |
| [Lpipe Improvements](#lpipe-improvements) | 🟨 🧪 | |
| [SPIKE: FAAS](#spike-faas) | 🧪 |  |
| [SPIKE: AWS Service Mesh](#spike-aws-service-mesh) | 🧪 |  |
| [Deploy Horiz Scaled Prometheus on K8s](#deploy-horiz-scaled-prometheus-on-k8s) | 🧪 |  |
| [Deploy RTD at K8s](#deploy-rtd-on-k8s) | 🟥 |  |
| [Ship Logs to K8s](#ship-logs-to-k8s) | 🟥 |  |
| [Ship Metrics to K8s](#ship-metrics-to-k8s) | 🟥 |  |
| [Consolidate Grafana at K8s](#consolidate-grafana-at-k8) | 🟥 |  |

## Project Details

### Kill Chef

#### Reasoning & Benefits

* Chef is horrible to manage and is a relic of contractors of christmas past.
* These days, we manage services and their dependencies via terraform and docker.

#### Subtasks
| Task | 🕙 | Description | Blockers |
| -  | - | - | - |
| EPIC: [Elasticsearch Refactor](#elasticsearch-refactor) |  |  |  |
| EPIC: [Refactor CI/CD](#refactor-cicd) | | | |
| Eliminate old DEVPI | 1d | SSH in, check who's using it, and check long-running jobs. If someone's using it, this is blocked by artifactory. Otherwise, turn it off. | [Artifactory](#artifactory) |
| Eliminate old NPM |  | | [Artifactory](#artifactory) |
| Eliminate infra VM(s) | 1d | SSH in, check who's using it, and check long-running jobs. | |
| EPIC: [Refactor Bastion VPN](#refactor-bastion-vpn) |  | | |

---

### Refactor Bastion VPN

#### Reasoning & Benefits

* See [Kill Chef](#kill-chef)

#### Subtasks
| Task | 🕙 | Description | Blockers |
| - | - | - | - |
| Deploy Bastion VPN in a sustainable way | 3w | This may or may not be containerized, depending on what works best. Main goal is to remove this from Chef. | |

---

### Refactor CI/CD

#### Reasoning & Benefits

* See [Kill Chef](#kill-chef)
* Our current deployment requires a lot of manual maintenance.

#### Subtasks
| Task | 🕙 | Description | Blockers |
| - | - | - | - |
| SPIKE: Review satoshi GitlabCI runners and document what it would take to migrate | | | |
| Deploy new CI solution | 8w | | |
| Containerized build stages | | | |
| Automated CI garbage cleanup | | | |
| CI autoscaling | | | |

---

### Elasticsearch Refactor

#### Reasoning & Benefits

* See [Kill Chef](#kill-chef)
* Our (old) elasticsearch deployment is extremely cost-inefficient.
* Our (old) cluster has poor monitoring and configuration management.

#### Subtasks
| Task | 🕙 | Description | Blockers |
| - | - | - | - |
| Ship logs to the new cluster | 1d | | |
| Setup Snapshots | 3d | Create snapshot configurations via `elasticsearch-artifacts` | |
| Create Alerts | 2d | Check and/or update alerts as necessary to catch edge cases we've identified during testing. | |
| Auto-rollover log indices at 2w | 1d | | |
| Rewrite logstash tempaltes for new ES | 1w | Update templates and possibly 🧪*Experiment*🧪 with elasticsearch pipelines. | |
| Release to Production | 1d | | All other tasks on this epic. |

---

### Elasticsearch CI/CD

#### Reasoning & Benefits

* Our new elasticsearch cluster would be even better if artifacts, mappings, snapshots, lifecycle management, and dashboards were version controlled and deployed via CI/CD.

#### Subtasks
| Task | 🕙 | Description | Blockers |
| - | - | - | - |
| Refactor `elasticsearch-artifacts` as terraform | 2w | 🧪*Experiment*🧪 Estimate is a guesstimate. | [Terraform CI/CD](#terraform-cicd), [Elasticsearch Refactor](#elasticsearch-refactor) |
| Deploy snapshots | ? | | |
| Deploy lifecycle management | ? | | |
| Deploy mappings | ? | | |
| Deploy kibana dashboards | ? | | |

---

### Logging Refactor

Blocked by [Elasticsearch Refactor](#elasticsearch-refactor)

#### Reasoning & Benefits

* Logging should require close to zero maintenance from SRE staff.
* Logs have been unreliable. A new cluster is part of the solution, but the other part is either raising the reliability of logstash or replacing it.

#### Subtasks
| Task | 🕙 | Description | Blockers |
| - | - | - | - |
| SPIKE: Elasticsearch Pipelines | 2w | 🧪*Experiment*🧪 Switch from filebeat, logstash, elasticsearch to filebeat, elasticsearch for application logs if elastic common schema allows it. |  |
| Update any non-conforming services | ? | | |
| Update MLL to enforce some schema | ? | | |

---

### Update Omni ES Deployment

#### Reasoning & Benefits

* We have a new elasticsearch cluster module which we should use to take advantage of those cost savings and scalability.

#### Subtasks
| Task | 🕙 | Description | Blockers |
| - | - | - | - |
| Deploy new cluster for Web | 2d | | |
| Update replication to use new clsuter | 2d | | |
| Update analytics service to use new cluster | 1d | | |

---

### Setup Tracing Service

#### Reasoning & Benefits

* We want better visibility into the movement of data points throughout our stack.

#### Subtasks
| Task | 🕙 | Description | Blockers |
| - | - | - | - |
| SPIKE: Tracing Services | 1w | 🧪*Experiment*🧪 Try tracing services and pick one (opentracing or opencensus, since they're merging into opentelemetry). | |
| Implement tracing | 3w | Setup tracing throughout our stack. Will likely require updating every service that needs tracing. | |

---

### Horizontally Scaled Prometheus

#### Reasoning & Benefits

* Our prometheus deployment is currently based on EFS which is ok for small deployments, but which we are close to toppling.

#### Subtasks
| Task | 🕙 | Description | Blockers |
| - | - | - | - |
| SPIKE: Horizontally Scaled Prometheus | 1w | 🧪*Experiment*🧪 Try horizontally scalable prometheus projcets (like thanos or cortex) and pick one. | |
| Deploy Horizontally Scaled Metrics | 2w | | |
| Drop high cardinality metrics | 2d | References [Prometheus Relabeling](https://medium.com/quiq-blog/prometheus-relabeling-tricks-6ae62c56cbda), [Dropping Tags](https://stackoverflow.com/questions/57497464/drop-a-prometheus-label-tag), [Dropping Tags 2](https://stackoverflow.com/questions/40376937/prometheus-how-to-drop-a-target-based-on-consul-tags) | |
| Alert on high-cardinality metrics | 3d | | |

---

### Monitoring Visibility

#### Reasoning & Benefits

* Stakeholders throughout our org want and need complete transparency into issues which impact the performance, throughput, or availability of our services.

#### Subtasks
| Task | 🕙 | Description | Blockers |
| - | - | - | - |
| Build unified platform status page | 1w | Use opsgenie or grafana to build a system/service health page which automagically demonstrates downtime based on acked alerts and system health metrics. | |
| Build release/deployment/health/errors dashboard | 1w | Provide visibility into system health with just enough granularity to be useful by engineering, pd, and other stakeholders. | |
| Terraform Release Logs | 2d | Ship a structured log every time soneone applies terraform. | [Refactor TF CLI](#refactor-tf-cli) |
| CI/CD Release Logs | 2d | Ship a structured log every time someone releases to any environment. | |

---

### Monitoring Alerting

#### Reasoning & Benefits

* Specific alerts are owned by squads, and they need guidance on how to write them.

#### Subtasks
| Task | 🕙 | Description | Blockers |
| - | - | - | - |
| Document how to write specific alerts | 1d | | |
| Monitor user experience | ? | 🧪*Experiment*🧪 Work with bravo to identify metrics we can watch which evaluate the human experience. | |
| Instrument services to expose endpoint request times | 2w | 🧪*Experiment*🧪 Find and standardize a way for us to track endpoint response times.| |

---

### Development Tooling

#### Reasoning & Benefits

* Abstract constantly copied development code into a shared tools to reduce (or eliminate) outdated projects.

#### Subtasks
| Task | 🕙 | Description | Blockers |
| - | - | - | - |
| Refactor local auth against AWS | 2w | Update to current standards, use keychains on macos & linux for on-screen prompts | |
| `build-harness`: Package management | 1w | Make package installation automagic when unavailable locally | |
| `build-harness`: Go commands | ? | 🧪*Experiment*🧪 Examine our existing golang services to see if the build-harness can be used in place of existing build commands. | |
| `build-harness`: Documentation | 1w | Documentation is required to make the `build-harness` accessible and accepted by genpop | |

---

### Git Tooling

#### Reasoning & Benefits

* Code ownership and etiquette are some of the biggest points of contention between squads, especially between BDP and GDA. We should have tools which automatically facilitate visibility into and accountability for codebase changes.

#### Subtasks
| Task | 🕙 | Description | Blockers |
| - | - | - | - |
| Create bot to notify codeowners of MRs | 1w | Notify codeowners in the flow if an MR is merged without a codeowner approval. | |
| Create bot to notify if codeowners is out of date | 1w | Notify the team periodically (via email? on MR open?) of repositories where the codeowners file is out of date. | |
| Provision gitlab repositories via terraform | 2w | 🧪*Experiment*🧪 We want to standardize our repo settings, especially the ones we all agree on or require (like deployment keys.) | |

---

### Refactor TF CLI

#### Reasoning & Benefits

* Our implementation is archaic and out-of-date.
* Refactoring will allow us access to better authentication and CI/CD

#### Subtasks
| Task | 🕙 | Description | Blockers |
| - | - | - | - |
| Refactor fabric/py2 into terragrunt/click/py3 | 2w | | [Development Tooling: Refactor local auth](#development-tooling) |

---

### Upgrade to TF 0.12

#### Reasoning & Benefits

* Upgrading will significantly reduce the complexity of our terraform code.
* Upgrading (and performing all the other TF maintenance listed in this planning document) will allows us to begin CI and maybe even CD of our infrastructure code.

#### Subtasks
| Task | 🕙 | Description | Blockers |
| - | - | - | - |
| Upgrade to TF 0.12 | 4w | TF 0.11 and 0.12 can live together, so if all goes well, this can be a gradual upgrade once the necessary framework is in place. | |

---

### Terraform CI/CD

Blocked by [Upgrade to TF 0.12](#upgrade-to-tf-012) and [Refactor TF CLI](#refactor-tf-cli)

#### Reasoning & Benefits

* Give us confidence that our changes are working - especially junior developers or those unfamiliar with terraform.

#### Subtasks
| Task | 🕙 | Description | Blockers |
| - | - | - | - |
| Automatically plan TF via Jenkins | 2w | Define ordering of layers, rely on terragrunt, fix resources that are different on every `apply`. | |
| Automatically apply TF via Jenkins | ? | 🧪*Experiment*🧪 | |

---

### Organize AWS Accounts

#### Reasoning & Benefits

* Cost management and infrastructure as code will be simpler as a result.

#### Subtasks
| Task | 🕙 | Description | Blockers |
| - | - | - | - |
| Create global/shared TF for all accounts | | | |

---

### Create AWS Budgets

#### Reasoning & Benefits

* We should automatically prevent our infrastructure from bankrupting us overnight.

#### Subtasks
| Task | 🕙 | Description | Blockers |
| - | - | - | - |
| Create AWS budgets | 1w | Create [AWS budgets](https://aws.amazon.com/aws-cost-management/aws-budgets/) for critical resources (especially elasticsearch) | |

---

### Create AWS Reports

#### Reasoning & Benefits

* We should automatically identify and remediate known current and potential issues.

#### Subtasks
| Task | 🕙 | Description | Blockers |
| - | - | - | - |
| Automatically remediate cloudwatch log groups | 2d | If a cloudwatch log group has no expiration, use a lambda (maintenance lambda?) to enforce a 14d expiration. | |
| SPIKE: AWS Reports | ? | 🧪*Experiment*🧪 Look for additional use cases where we could automatically remediate or alert on meta problems. | |

---

### Terraform Maintenance

#### Reasoning & Benefits

* We need to pay down technical debt in order to provide better transparency into cost and better component isolation for testing and deployment.

#### Subtasks
| Task | 🕙 | Description | Blockers |
| - | - | - | - |
| TF Tag Enforcement | 3d | Use aws reports to discover untagged resources | |
| Kill `var.stage` | 1w | | |
| Unify internal Route53 | 2w | | |
| Reverse load-balancer dependencies | 1w | | |

---

### Terraform Unit Testing

#### Reasoning & Benefits

* I shouldn't need to explain why unit testing is *required*. 😛

#### Subtasks
| Task | 🕙 | Description | Blockers |
| - | - | - | - |
| Write TF Unit Tests | ? | 🧪*Experiment*🧪 and isolate components | |

---

### Terraform Cookiecutter

#### Reasoning & Benefits

* Developers who don't know terraform shouldn't need to go through multiple iterations of MRs just to get the most basic code in-place.
* We should provide almost everything you need to create the infrastructure for your service with minimal effort from the invididual developer.

#### Subtasks
| Task | 🕙 | Description | Blockers |
| - | - | - | - |
| Cookiecutter ALB Service | 1d | | |
| Cookiecutter NLB Service | 1d | | |
| Cookiecutter Lambda | 1d | | |
| Update ALB Services to match cookiecutter | 1d | | |
| Update NLB Services to match cookiecutter| 1d | | |
| Update Lambdas to match cookiecutter | 1d | | |

---

### Terraform ECS Autoscaling

#### Reasoning & Benefits

* We are currently over-paying by using more EC2 instances than necessary.
* The stack should grow on-demand according to the amount of work being done.

#### Subtasks
| Task | 🕙 | Description | Blockers |
| - | - | - | - |
| Autoscale ECS Clusters | 1w | Refactor our clusters to autoscale (use ASGs). | |
| Autoscale ECS Tasks | 1w | Attach autoscaler modules to every ECS service (if possible.) | |

---

### Terraform SQS Autoscaler

#### Reasoning & Benefits

* We already have an autoscaler for memory and CPU, this would mirror that module.
* Scaling on queue size at the service/task level provides a very transparent way for developers to take full advantage of the hardware at their disposal.

#### Subtasks
| Task | 🕙 | Description | Blockers |
| - | - | - | - |
| Create SQS Autoscaler Module | 3d | Scale an ECS service on reported volume in an SQS queue. | |

---

### Cloud Gateways And IDP

#### Reasoning & Benefits

* De-isolating our cloud and on-prem environments requires standardized entrypoints, and IDPs which meet industry standards.

#### Subtasks
| Task | 🕙 | Description | Blockers |
| - | - | - | - |
| SPIKE: API Gateways | 2w | 🧪*Experiment*🧪 In conjunction with SRE & Infra, pick an API gateway solution and intermediary IDP. | |
| SPIKE: IDPs | 2w | 🧪*Experiment*🧪 | |

---

### Provision Opsgenie via TF

#### Reasoning & Benefits

* Opsgenis is currently configured by hand, but they have an extremely good API, and we could provision it via terraform.

#### Subtasks
| Task | 🕙 | Description | Blockers |
| - | - | - | - |
| Provision Opsgenie via terraform | 2w | Use old providers or write our own to provision desired opsgenie resources via terraform. | |

---

### Artifactory

#### Reasoning & Benefits

* The cost/benefit of managing/hosting devpi and NPM ourselves is greatly outweighed by letting someone else do it better.
* This is still in testing.

#### Subtasks
| Task | 🕙 | Description | Blockers |
| - | - | - | - |
| Locally authenticate against Artifactory | 2w | Update local development workflow against package repos to acccept authenticated connections on a per-user basis. | [Development Tooling: Refactor local auth](#development-tooling) |
| `jsl`: Publish devpi packages to artifactory | 1d |  |  |

---

### SPIKE: Integration Testing

#### Reasoning & Benefits

* We need to bring our holistic testing standards up to par.
* We should test that infrastructure is in place before sending a deployment live.

#### Subtasks
| Task | 🕙 | Description | Blockers |
| - | - | - | - |
| Update an ECS service to standardize testing and integration test | | | |
| Update a Lambda to standardize testing and integration test | | | |

---

### Accessible Documentation

#### Reasoning & Benefits

* Self-explanitory

#### Subtasks
| Task | 🕙 | Description | Blockers |
| - | - | - | - |
| Deploy Readthedocs | 1w | Off the shelf images are insufficient. Create a future-ready Mintel RTD image and deploy. |  |
| JSL: Create deployment process for service documentation | 1w | | |
| Auto-deploy TF readmes to RTD| 1d | | [Terraform CI/CD](#terraform-cicd) |
| Update onboarding docs for new laptops | 1d | | |
| Document our logging stack | 1d | | |
| Document elasticsearch | 2d | | |
| Document gateway | 1d | | |
| Document coreOS | 2d | | |

---

### Testing Methodology

Blocked by [SPIKE: Integration Testing](#spike-integration-testing)

#### Reasoning & Benefits

* We need to bring our holistic testing standards up to par.

#### Subtasks
| Task | 🕙 | Description | Blockers |
| - | - | - | - |
| Define Terminology | 2d | Define our standard testing terminology with the help of other squads. Further meetings required. Document this in Confluence/RTD. | |
| Define Methodology | 2w | Discovery all methods of testing used on the BDP and boil them down to the best methodologies. Define high-level testing methodology and document specific examples for certian technologies. This will be an ongoing task for our team beyond the initial version. | [SPIKE: Integration Testing](#spike-integration-testing) |
| Unify testing libraries | 2w | Examine librarie used by our testing stack(s) and unify them if possible. Suggested target is `pytest-everest` | |

---

### Lpipe Improvements

#### Reasoning & Benefits

* lambda-pipeline (`lpipe`) provides a holistic framework for developing directed-graph workflows via AWS Lambda with minimal effort and minimal code-duplication by developers.

#### Subtasks
| Task | 🕙 | Description | Blockers |
| - | - | - | - |
| Use Pydantic models to validate input | ? | 🧪*Experiment*🧪 | |
| Implement protobuf or jsonschemas in lpipe | ? | 🧪*Experiment*🧪 | |
| Lambda registry | ? | 🧪*Experiment*🧪 | |
| Move lpipe to Github & travisCI | 1w | Moving is easy, the rest of the time will be extracting Mintel/BDP dependencies that have crept in. | |

---

### SPIKE: FAAS

#### Reasoning & Benefits

* 

#### Subtasks
| Task | 🕙 | Description | Blockers |
| - | - | - | - |
| | | | |

---

### SPIKE: AWS Service Mesh

#### Reasoning & Benefits

* 

#### Subtasks
| Task | 🕙 | Description | Blockers |
| - | - | - | - |
| | | | |

---

### Deploy Horiz Scaled Prometheus on K8s

#### Reasoning & Benefits

* 

#### Subtasks
| Task | 🕙 | Description | Blockers |
| - | - | - | - |
| Deploy Horizontally Scaled Metrics at K8s | | | [Horizontally Scaled Prometheus](#horizontally-scaled-prometheus) |

---

### Deploy RTD at K8s

#### Reasoning & Benefits

* 

#### Subtasks
| Task | 🕙 | Description | Blockers |
| - | - | - | - |
| | | | |

---

### Ship Logs to K8s

#### Reasoning & Benefits

* 

#### Subtasks
| Task | 🕙 | Description | Blockers |
| - | - | - | - |
| | | | |

---

### Ship Metrics to K8s

#### Reasoning & Benefits

* 

#### Subtasks
| Task | 🕙 | Description | Blockers |
| - | - | - | - |
| | | | |

---

### Consolidate Grafana at K8s

#### Reasoning & Benefits

* 

#### Subtasks
| Task | 🕙 | Description | Blockers |
| - | - | - | - |
| | | | |